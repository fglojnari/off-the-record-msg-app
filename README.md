# off-the-record-msg-app

<h3>Description</h3>

Mobile application written in <a href="https://ionicframework.com/">ionic</a> (frontend) and <a href="https://expressjs.com/">express.js</a> (backend) for the purpose of 
understanding OTR protocol for end-to-end encryption in sending text messages. This app is written for learning purposes only and therefore this code should not be used in 
production. We use server only to route the message to the right client.

<h3>Workflow</h3>

    1. Diffie-Hellman key exchange (on every message exchange we generate new private key for RC4 encryption)
    2. RSA for first message authentication
    3. Symmetric RC4 chiper is used to encrypt the message that is sent to the other client
    4. Using MAC for checking the origin of the message
    
