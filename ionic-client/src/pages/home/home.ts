import { Component, HostListener } from '@angular/core';
import {$WebSocket} from 'angular2-websocket/angular2-websocket';
import { NavController } from 'ionic-angular';
import * as bigint from 'big-integer';
import { BigInteger } from 'big-integer';

import * as CryptoRSA from 'crypto-browserify';

import { Sha1 } from '../../assets/Sha1';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public username = '';
  private myPublicKey: BigInteger;
  public selectedClient = '';
  public selectedClientKey: BigInteger;
  private commonKey = '';
  private chatId = -1;
  private userType = 'none';
  public step = '';
  public replyOccured = null;
  public HMAC = '';
  
  public text = '';
  public inputText = '';
  public ws: $WebSocket;
  public clients = [];

  public secret;
  private minSecret = 2210312426921032588552076022197566074856950548502459942654116941958108831682612228890093858261341614673227141477904012196503648957050582631942730706805009223062734745341073406696246014589361659774041027169249453200378729434170325843778659198143763193776859869524088940195577346119843545301547043747207749969;
  private maxSecret = 2310312426921032588552076022197566074856950548502459942654116941958108831682612228890093858261341614673227141477904012196503648957050582631942730706805009223062734745341073406696246014589361659774041027169249453200378729434170325843778659198143763193776859869524088940195577346119843545301547043747207749969;

  public privateKeyFirstClient = `-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgFoYffAOxG8tfhbeZoWTmusF9/mFRiMQqbDane3LbJUw9fxDfX7G
1C8ckrmlugjZIPKQYvPrWiFuC62oirwqlJWEfecUGmp+LSXdEglrJt6fF+t51jpl
Hv7INsvzEk5gyWN1d9BFN+6XL/GnI5JS5cwBCD6VOd+JQnnDAp4XfAW1AgMBAAEC
gYAXAeonQ54mRhfM/z5vHiKRohSRQ9eFXNv4rdtmvmwcLUSkBxglJ+FHpqRRhGDd
oZUoXHbok1GQ9+lDj6beJoGK3ntPwwMIZvUv70Hr3Pd3MSJKSPoR0b08/VuJ2swz
LyNixgoaH68QtKnW6nAlqpyZhcramQWzdyg7MTDZiVUwgQJBALPzbs50+iIcLb0F
6em0WjyA/4wnrg5PL6IBjBsyFY6uxDEFSmRh0s6oP04qqw2sRRsS2pjJ1reO4a2W
KymsincCQQCAK8gVY6xpzwc7L3DG5zIFMqXCimfvyjMWK1lxudQO8biTi/k/Konc
MV7TPDSwYxHgBI8AWVPCGpgvLpwicxAzAkEAiximFAwztN4XNL83eiCgh5gm8E1T
q1fmN9H+XJ8wLqREnqe6QzGiY3+Fv4QXyG/a0vyV3/hUcnqNU0J6hrIKLQJAAIKs
IFGuTRmS+MxekZw4Gccy28x7T3wpqevwRkCsNIcH6iaDjpnXxLW5n/O8bftnBdJe
02yHpd0/P+janFSC+wJBAJyU/8SF4jQr8uZ3TddAKu0qstjiLaBWH25bnhMHG+Yd
+MUnAyfCa2Exs2WyCyihrlnhXO5P2uReXJbOUiGuz90=
-----END RSA PRIVATE KEY-----`;
public publicKeyFirstClient = `-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgFoYffAOxG8tfhbeZoWTmusF9/mF
RiMQqbDane3LbJUw9fxDfX7G1C8ckrmlugjZIPKQYvPrWiFuC62oirwqlJWEfecU
Gmp+LSXdEglrJt6fF+t51jplHv7INsvzEk5gyWN1d9BFN+6XL/GnI5JS5cwBCD6V
Od+JQnnDAp4XfAW1AgMBAAE=
-----END PUBLIC KEY-----`;
  
  //128
  //private p: BigInteger = bigint('197221152031991558322935568090317202983');
  //256
  //private p: BigInteger = bigint('112457129983317064494133258034491756790943511028023366901014968560410379195027');
  //512
  //private p: BigInteger = bigint('8559108389020329242292975344831200201322770016424405688376743638778039881377148946947806398238584041519440199927822505853256616891684977416276395927762259');
  //1024
  //private p: BigInteger = bigint('107560960624254990201058455140013217069949331248114341574298248597055584898126321093330768462486995125563469663163251404817709163750190539811190838598297618647549202383604617467114600875710464310946354071436349896908521522241557852517013870821005750869709992636644149420326609392283245879753842682719140993487');  
  //1536
  private p: BigInteger = bigint('2410312426921032588552076022197566074856950548502459942654116941958108831682612228890093858261341614673227141477904012196503648957050582631942730706805009223062734745341073406696246014589361659774041027169249453200378729434170325843778659198143763193776859869524088940195577346119843545301547043747207749969763750084308926339295559968882457872412993810129130294592999947926365264059284647209730384947211681434464714438488520940127459844288859336526896320919633919');
  //2048
  //private p: BigInteger = bigint('32317006071311007300338913926423828248817941241140239112842009751400741706634354222619689417363569347117901737909704191754605873209195028853758986185622153212175412514901774520270235796078236248884246189477587641105928646099411723245426622522193230540919037680524235519125679715870117001058055877651038861847280257976054903569732561526167081339361799541336476559160368317896729073178384589680639671900977202194168647225871031411336429319536193471636533209717077448227988588565369208645296636077250268955505928362751121174096972998068410554359584866583291642136218231078990999448652468262416972035911852507045361090559');
  //3072
  //private p: BigInteger = bigint('5809605995369958062791915965639201402176612226902900533702900882779736177890990861472094774477339581147373410185646378328043729800750470098210924487866935059164371588168047540943981644516632755067501626434556398193186628990071248660819361205119793693985433297036118232914410171876807536457391277857011849897410207519105333355801121109356897459426271845471397952675959440793493071628394122780510124618488232602464649876850458861245784240929258426287699705312584509625419513463605155428017165714465363094021609290561084025893662561222573202082865797821865270991145082200656978177192827024538990239969175546190770645685893438011714430426409338676314743571154537142031573004276428701433036381801705308659830751190352946025482059931306571004727362479688415574702596946457770284148435989129632853918392117997472632693078113129886487399347796982772784615865232621289656944284216824611318709764535152507354116344703769998514148343807');
  //4096
  //private p: BigInteger = bigint('1044388881413152506679602719846529545831269060992135009022588756444338172022322690710444046669809783930111585737890362691860127079270495454517218673016928427459146001866885779762982229321192368303346235204368051010309155674155697460347176946394076535157284994895284821633700921811716738972451834979455897010306333468590751358365138782250372269117968985194322444535687415522007151638638141456178420621277822674995027990278673458629544391736919766299005511505446177668154446234882665961680796576903199116089347634947187778906528008004756692571666922964122566174582776707332452371001272163776841229318324903125740713574141005124561965913888899753461735347970011693256316751660678950830027510255804846105583465055446615090444309583050775808509297040039680057435342253926566240898195863631588888936364129920059308455669454034010391478238784189888594672336242763795138176353222845524644040094258962433613354036104643881925238489224010194193088911666165584229424668165441688927790460608264864204237717002054744337988941974661214699689706521543006262604535890998125752275942608772174376107314217749233048217904944409836238235772306749874396760463376480215133461333478395682746608242585133953883882226786118030184028136755970045385534758453247');

  private g = 2;
  public requestMsg = '';

  public connected = false;

  constructor(public navCtrl: NavController) {

  }

  calc(val, exp, mod) {
    return bigint(val).modPow(exp, mod);
  }

  connect() {
    this.ws = new $WebSocket('ws://localhost:3000');
        this.ws.onMessage(
            (msg: MessageEvent) => {
              const parts = msg.data.split('#');
              switch (parts[0]) {
                case 'clients': {
                  const allClients: string[] = JSON.parse(parts[1]);
                  this.clients = allClients.filter( client => (client != this.username));
                  this.selectedClient = this.clients[0];
                  this.connected = true;
                  break;
                }
                case 'gn': {
                  this.chatId = parts[5];
                  if (parts[4] == this.username) {
                    this.userType = 'guest';
                    this.requestMsg = `${parts[3]} sent you a private chat request, confirm or decline the request!`;
                    this.step = 'privChatRequest';
                    this.selectedClient = parts[3];
                    this.secret = this.getRandomInt(this.minSecret,this.maxSecret);
                  } else if (parts[3] == this.username) {
                    this.userType = 'inviter';
                    this.step = 'inviting';
                    this.selectedClient = parts[4];
                    this.secret = this.getRandomInt(this.minSecret,this.maxSecret);
                    this.myPublicKey = this.calc(this.g, this.secret, this.p);
                    //Authentication, signig first msg
                    var method = CryptoRSA.createSign('RSA-SHA512');
                    method.update(this.myPublicKey);
                    var signedText = method.sign(this.privateKeyFirstClient, 'hex');

                    this.ws.send4Direct(`exchangeKeys#${this.chatId}#${this.myPublicKey.toString()}#${signedText}`); // Alice to bob
                  }
                  break;
                }
                case 'closePrivChat': {
                  this.step = 'homePage';
                  this.chatId = 0;
                  this.secret = '';
                  this.selectedClientKey = null;
                  this.myPublicKey = null;
                  this.userType = 'none';
                  this.commonKey = '';
                  this.text = '';
                  break;
                }
                case 'exchangeKeysAnswer': { //Bob and alice recieve public key
                  this.selectedClientKey = bigint(parts[1]);

                  //Verifying signature
                  var methodD = CryptoRSA.createVerify('RSA-SHA512');
                  methodD.update(this.selectedClientKey);
                  if(!methodD.verify(this.publicKeyFirstClient, parts[2], 'hex')) {
                    console.log("Man in the middle attack occured, closing chat session!")
                    this.ws.send4Direct(`closePrivChat#${this.chatId}`);
                  } else {
                    console.log("Authentication succeded!");
                  }

                  if (this.myPublicKey && this.selectedClientKey) {
                    this.step = 'privateChatting';
                    this.commonKey = this.calc(this.selectedClientKey, this.secret, this.p).toString();
                    console.log(this.commonKey);
                  }

                  break;
                }
                case 'msg': { // Sprejemanje sporocila
                  const clientsKey = bigint(parts[1]);
                  const senderName = parts[2];
                  const text = parts[3];
                  const hmacFromSender = parts[4];
                  this.replyOccured = true;
                  this.selectedClientKey = clientsKey;
                  this.commonKey = this.calc(this.selectedClientKey, this.secret, this.p).toString();

                  var reciverHmac = Sha1.hash(parts[1] + text + this.commonKey);
                  if(!reciverHmac==hmacFromSender) {
                    console.log("Man in the middle attack occured, closing chat session!")
                    this.ws.send4Direct(`closePrivChat#${this.chatId}`);
                  } else {
                    console.log("Authentication succeded!")
                  }
                  console.log('Decrypted with: ' + this.commonKey);
                  this.text += senderName+': '+this.rc4(this.commonKey,text)+'\n';
                }
              }
            },
            {autoApply: false}
        );

    this.ws.send4Direct(`conn#${this.username}`);
    this.step = 'homePage';
  }

  send(type, data) {
    switch (type) {
      case 'initPrivChat': {
        this.ws.send4Direct(`initPrivChat#${this.username}#${this.selectedClient}`);
        break;
      }
      case 'exchangeKeys': { // Bob to Alice
        this.myPublicKey = this.calc(this.g, this.secret, this.p);

        //Authentication, signing first message
        var method = CryptoRSA.createSign('RSA-SHA512');
        method.update(this.myPublicKey);
        var signedText = method.sign(this.privateKeyFirstClient, 'hex');

        this.ws.send4Direct(`exchangeKeys#${this.chatId}#${this.myPublicKey.toString()}#${signedText}`);

        if (this.myPublicKey && this.selectedClientKey) {
          this.step = 'privateChatting';
          this.commonKey = this.calc(this.selectedClientKey, this.secret, this.p).toString();
          console.log(this.commonKey);
        }
        break;
      }
      case 'closePrivChat': {
        this.ws.send4Direct(`closePrivChat#${this.chatId}`);
        break;
      }
      default: { //Posiljanje sporocila
        if(this.inputText != ''){
          if(this.replyOccured){
            console.log('generating new PK');
            this.secret = this.getRandomInt(this.minSecret,this.maxSecret);
            this.myPublicKey = this.calc(this.g, this.secret, this.p);
            this.commonKey = this.calc(this.selectedClientKey, this.secret, this.p).toString();
          }
          this.HMAC = Sha1.hash(this.myPublicKey.toString()+this.rc4(this.commonKey,this.inputText)+this.commonKey);
          console.log('Encrypted with: ' + this.commonKey);
          this.ws.send4Direct(`msg#${this.myPublicKey.toString()}#${this.chatId}#${this.username}#${this.rc4(this.commonKey,this.inputText)}#${this.HMAC}`);
          this.text += this.username+': '+this.inputText+'\n';
          this.replyOccured = false;
          this.inputText = '';
        }
      }
    }
  }

   getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  ngOnDestroy() {
    this.ws.close(true);
  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHander(event) {
    this.ws.close(true);
  }

 rc4(key, str) {
    var s = [], j = 0, x, res = '';
    for (var i = 0; i < 256; i++) {
      s[i] = i;
    }
    for (i = 0; i < 256; i++) {
      j = (j + s[i] + key.charCodeAt(i % key.length)) % 256;
      x = s[i];
      s[i] = s[j];
      s[j] = x;
    }
    i = 0;
    j = 0;
    for (var y = 0; y < str.length; y++) {
      i = (i + 1) % 256;
      j = (j + s[i]) % 256;
      x = s[i];
      s[i] = s[j];
      s[j] = x;
      res += String.fromCharCode(str.charCodeAt(y) ^ s[(s[i] + s[j]) % 256]);
    }
    return res;
  }

}
