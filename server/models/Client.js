class Client {
    constructor(ws, ip, name){
        this.ws = ws;
        this.ip = ip;
        this.name = name;
    }
}

module.exports = {
    Client
};