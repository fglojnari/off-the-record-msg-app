let Client = require('./Client').Client;

class PrivateChat {
    constructor(client1, client2){
        this.client1 = client1;
        this.client2 = client2;
        this.n = this.randomPrime();
        this.g = 2;
    }

    randomPrime() {
        //"random" 51455639, 786431
        return '51455639'; 
    }

    generateN(){
        let x = '';
        let min = Math.ceil(0);
        let max = Math.floor(9);
        //size is 1536
        //So that first one isnt 0
        x += Math.floor(Math.random() * (max - 1 + 1)) + 1 +'';
        for(var i = 0; i < 1535; i++)
        {
            x += Math.floor(Math.random() * (max - min + 1)) + min +'';
        }
        return x;
    }
}

module.exports = {
    PrivateChat
};