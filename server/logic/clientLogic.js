let Client = require('../models/Client').Client;

let clients = [];

class ClientLogic {

    static add(ws, ip, name){
        clients.push(new Client(ws, ip, name));
    }

    static exists(ip, name){
        if(clients.find( client =>  (ip == client.ip && name == client.name)))
            return true;
        else
            return false;
    }

    static getClientNamesJson(){
        let clientsNames = [];
        for(var i = 0; i < clients.length; i++)
            clientsNames[i] = clients[i].name;
            
        return JSON.stringify(clientsNames);
    }

    static find(name){
        return clients.find( client => (client.name == name));
    }
}

module.exports = {
    ClientLogic
};