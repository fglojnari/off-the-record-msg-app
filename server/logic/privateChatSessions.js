let Client = require('../models/Client').Client;
let PrivateChat = require('../models/PrivateChat').PrivateChat;

let sessions = [];

class PrivateChatSessions {

    static add(client1, client2){
        sessions.push(new PrivateChat(client1, client2));
        return sessions[sessions.length - 1];
    }

    static delete(id){
        sessions.splice(id, 1);
    }

    static get(id){
        return sessions[id];
    }

    static exists(ip, name){
        if(clients.find( client =>  (ip == client.ip && name == client.name)))
            return true;
        else
            return false;
    }

    static length(){
        return sessions.length;
    }

}

module.exports = {
    PrivateChatSessions
};